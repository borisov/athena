
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "TruthRivetTools/HiggsTruthCategoryTool.h"

DECLARE_TOOL_FACTORY( HiggsTruthCategoryTool )

DECLARE_FACTORY_ENTRIES( TruthRivetTools ) 
{
  DECLARE_TOOL( HiggsTruthCategoryTool )
}
