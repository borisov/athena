/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#ifndef ZDCANALYSIS_ZDCANALYSIS_DICT_H
#define ZDCANALYSIS_ZDCANALYSIS_DICT_H

#include <ZdcAnalysis/ZdcAnalysisAlg.h>
#include <ZdcAnalysis/ZdcAnalysisTool.h>

#endif
