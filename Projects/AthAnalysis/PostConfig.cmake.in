#
# File taking care of pointing the downstream projects at the right
# version of the externals.
#

# Make sure that an RPM dependency is set up for the correct version of
# Gaudi:
set( GAUDI_VERSION @GAUDI_VERSION@
   CACHE STRING "Version of Gaudi used for the RPM dependency setup" )

# Find Gaudi:
find_package( Gaudi REQUIRED )

# Load all the files from the externals/ subdirectory:
get_filename_component( _thisdir ${CMAKE_CURRENT_LIST_FILE} PATH )
file( GLOB _externals "${_thisdir}/externals/*.cmake" )
unset( _thisdir )
foreach( _external ${_externals} )
   include( ${_external} )
   get_filename_component( _extName ${_external} NAME_WE )
   string( TOUPPER ${_extName} _extNameUpper )
   if( NOT AthAnalysis_FIND_QUIETLY )
      message( STATUS "Taking ${_extName} from: ${${_extNameUpper}_ROOT}" )
   endif()
   unset( _extName )
   unset( _extNameUpper )
endforeach()
unset( _external )
unset( _externals )

# This is an analysis project, so set the appropriate compile flags:
add_definitions( -DXAOD_ANALYSIS )
add_definitions( -DROOTCORE_RELEASE_SERIES=$ENV{ROOTCORE_RELEASE_SERIES} )

# And some variables for CMake as well:
set( XAOD_ANALYSIS TRUE CACHE BOOL
   "Flag specifying that this is an analysis release" )
