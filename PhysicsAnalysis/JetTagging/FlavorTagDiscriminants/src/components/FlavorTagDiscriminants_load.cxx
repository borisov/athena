/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "GaudiKernel/LoadFactoryEntries.h"

LOAD_FACTORY_ENTRIES(FlavorTagDiscriminantsLib)
