#include "GaudiKernel/DeclareFactoryEntries.h"
#include "DerivationFrameworkSM/PhotonVertexSelectionWrapper.h"
using namespace DerivationFramework;

DECLARE_TOOL_FACTORY( PhotonVertexSelectionWrapper )
DECLARE_FACTORY_ENTRIES( DerivationFrameworkSM ) {
    DECLARE_TOOL( PhotonVertexSelectionWrapper )
}
  
