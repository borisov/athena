# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#Content included in addition to the Smart Slimming Content

HDBS2ExtraContent=[
    "egammaClusters.rawE.phi_sampl.calM",
    "GSFTrackParticles.expectNextToInnermostPixelLayerHit.numberOfNextToInnermostPixelLayerHits.numberOfNextToInnermostPixelLayerOutliers",
    "Muons.quality.etcone20",
    "ExtrapolatedMuonTrackParticles.numberOfTRTHits.numberOfTRTOutliers",
    "AntiKt4EMTopoJets.JetEMScaleMomentum_pt.JetEMScaleMomentum_eta.JetEMScaleMomentum_phi.JetEMScaleMomentum_m"
    ]

HDBS2ExtraContentTruth=[
    "TruthEvents.PDFID1.PDFID2.PDGID1.PDGID2.Q.X1.X2.XF1.XF2.weights.crossSection",
    ]

HDBS2ExtraContainers=[]

HDBS2ExtraContainersTruth=[
    "TruthBosonWithDecayParticles", "TruthBosonWithDecayVertices"
    ]
