# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#Content included in addition to the Smart Slimming Content

HIGG2D2ExtraContent=[
    "egammaClusters.rawE.phi_sampl.calM",
    "GSFTrackParticles.expectNextToInnermostPixelLayerHit.numberOfNextToInnermostPixelLayerHits.numberOfNextToInnermostPixelLayerOutliers",
    "Muons.quality.etcone20.ptconecoreTrackPtrCorrection",
    "ExtrapolatedMuonTrackParticles.numberOfTRTHits.numberOfTRTOutliers",
    "AntiKt4EMTopoJets.JetEMScaleMomentum_pt.JetEMScaleMomentum_eta.JetEMScaleMomentum_phi.JetEMScaleMomentum_m",
    "TauJets.ptDetectorAxis,phiDetectorAxis,etaDetectorAxis,mDetectorAxis",
    "TauNeutralParticleFlowObjects.pt.eta.phi.m",
    "TauChargedParticleFlowObjects.pt.eta.phi.m"
    ]

HIGG2D2ExtraContentTruth=[
    ]

HIGG2D2ExtraContainers=[
    "MuonClusterCollection",
    "MET_Track",
    "CaloCalTopoClusters"]

HIGG2D2ExtraContainersTruth=[
    "TruthEvents",
    "TruthParticles",
    "TruthVertices",
    "AntiKt4TruthJets",
    "AntiKt4TruthWZJets",
    "MuonTruthParticles"]
