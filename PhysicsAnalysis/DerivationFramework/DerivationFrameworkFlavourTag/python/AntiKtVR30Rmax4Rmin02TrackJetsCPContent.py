# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

from JetFlavorGhostLabels import getJetFlavorGhostLabels

AntiKtVR30Rmax4Rmin02TrackJetsCPContent = [
"AntiKtVR30Rmax4Rmin02TrackJets",
"AntiKtVR30Rmax4Rmin02TrackJetsAux.pt.eta.phi.m.HadronConeExclExtendedTruthLabelID.HadronConeExclTruthLabelID.",
]
