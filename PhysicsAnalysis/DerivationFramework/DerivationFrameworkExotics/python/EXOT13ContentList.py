# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

EXOT13SmartContent = [
    "InDetTrackParticles",
    "Electrons",
    "Muons",
    "PrimaryVertices",
    "MET_Reference_AntiKt4EMTopo",
    "AntiKt4EMTopoJets",
    "AntiKt4TruthJets"
]

EXOT13AllVariablesContent = [
    "MuonSpectrometerTrackParticles",
    "MuonSegments",
    "ExtrapolatedMuonTrackParticles",
    "METAssoc_AntiKt4EMTopo",
    "MET_Core_AntiKt4EMTopo",
    "MET_Truth",
    "TruthEvents",
    "TruthVertices",
    "TruthParticles"
]

EXOT13ExtraVariables = [
    "AntiKt4EMTopoJets.HECQuality.sumpttrk.FracSamplingMax.NegativeE.AverageLArQF.FracSamplingMaxIndex.LArQuality.HECFrac.EMFrac.Width.Timing",
    "Muons.allAuthors.rpcHitTime.rpcHitIdentifier.rpcHitPositionX.rpcHitPositionY.rpcHitPositionZ",
    "CaloCalTopoClusters.e_sampl.calM.calE.calEta.calPhi",
    "Electrons.MediumLH.ptvarcone30",
]

EXOT13UnslimmedContent = []
