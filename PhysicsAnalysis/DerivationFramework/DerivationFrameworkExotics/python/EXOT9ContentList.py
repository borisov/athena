# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

EXOT9Content = []

EXOT9AllVariables = [
    "TruthParticles",
    "TruthEvents",
    "TruthVertices",
    "MET_Truth",
    "ExtrapolatedMuonTrackParticles",
    "CombinedMuonTrackParticles",
    "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_mht",
    "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_topocl_PUC"
]

EXOT9SmartCollections = [
    "Photons",
    "TauJets",
    "Electrons",
    "Muons",
    "MET_Reference_AntiKt4EMTopo",
    "MET_Reference_AntiKt4EMPFlow",
    "BTagging_AntiKt4EMTopo_201810",
    "BTagging_AntiKt4EMPFlow_201810",
    "BTagging_AntiKt4EMPFlow_201903",
    "AntiKt4EMTopoJets",
    "AntiKt4EMTopoJets_BTagging201810",
    "AntiKt4EMPFlowJets",
    "AntiKt4EMPFlowJets_BTagging201810",
    "AntiKt4EMPFlowJets_BTagging201903",
    "PrimaryVertices",
    "InDetTrackParticles"
]

EXOT9Extravariables = [
    "Muons.rpcHitTime.meanDeltaADCCountsMDT.MeasEnergyLoss",
    "InDetTrackParticles.numberOfIBLOverflowsdEdx.TRTdEdxUsedHits.TRTdEdx.numberOfTRTHighThresholdHitsTotal.numberOfTRTXenonHits.numberOfUsedHitsdEdx.pixeldEdx"
]
