#include "GaudiKernel/DeclareFactoryEntries.h"
#include "GammaORTools/VGammaORTool.h"

DECLARE_TOOL_FACTORY( VGammaORTool )
DECLARE_FACTORY_ENTRIES( GammaORTools ) 
{
  DECLARE_ALGTOOL( VGammaORTool )
}

