// Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#ifndef XAODEVENTSHAPE_XAODEVENTSHAPEDICT_H
#define XAODEVENTSHAPE_XAODEVENTSHAPEDICT_H

// Includes for the dictionary generation:
#include "xAODEventShape/versions/EventShape_v1.h"
#include "xAODEventShape/versions/EventShapeAuxInfo_v1.h"

#include "xAODEventShape/EventShape.h"
#include "xAODEventShape/EventShapeAuxInfo.h"

#endif // XAODEVENTSHAPE_XAODEVENTSHAPEDICT_H
